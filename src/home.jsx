import React from "react";

import Ctform from "./components/formct";
import Form from "./components/form";

class Home extends React.Component {
    render() {
        return (
            <div className="bg-dark" style={{ minHeight: "100vh" }}>
                <Ctform>
                    <Form setLogin={this.props.setLogin} />
                </Ctform>
            </div>
        );
    }
}

export default Home;
