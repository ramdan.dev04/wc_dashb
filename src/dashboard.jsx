import React from "react";
import Cookies from "universal-cookie";
const cookies = new Cookies();

class Dashboard extends React.Component {
    logOut = () => {
        cookies.remove("Auth");
        window.location.href = "../../";
    };

    componentDidMount = () => {
        let Auth = cookies.get("Auth");
        if (!Auth || Auth !== "Beta") {
            window.location.href = "../../";
        }
    };

    render() {
        return (
            <div>
                <h1>Dashboard</h1>
                <button onClick={this.logOut}>Log Out</button>
            </div>
        );
    }
}

export default Dashboard;
