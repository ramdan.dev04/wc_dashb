import React from 'react';
import { render } from 'react-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap';

import App from './app';

render(<App />, document.getElementById('root'));