import React from "react";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import Home from "./home";
import Dashboard from "./dashboard";

class App extends React.Component {
    render() {
        return (
            <BrowserRouter>
                <Switch>
                    <Route path="/dashboard">
                        <Dashboard />
                    </Route>
                    <Route exact path="/">
                        <Home />
                    </Route>
                </Switch>
            </BrowserRouter>
        );
    }
}

export default App;
