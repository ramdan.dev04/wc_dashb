import React from "react";
import Swal from "sweetalert2";
import Cookies from "universal-cookie";
let cookies = new Cookies();

class Form extends React.Component {
    licRef = React.createRef(null);
    state = {
        toast: true,
    };
    setToast = (value) => {
        let state = { ...this.state };
        state.toast = value;
        this.setState(state);
    };
    offToast = () => {
        let state = { ...this.state };
        state.toast = true;
        this.setState(state);
    };
    handleSubmit = (e) => {
        e.preventDefault();
        if (
            this.licRef.current.value === null ||
            this.licRef.current.value.length < 32
        ) {
            this.setToast(false);
        } else {
            this.setToast(true);
            let body = {
                license: this.licRef.current.value,
            };
            fetch("https://api-member.wizcard.co/login", {
                method: "POST",
                headers: {
                    "Content-Type": "application/json",
                },
                body: JSON.stringify(body),
            })
                .then((response) => response.json())
                .then((res) => {
                    if (res.status === 200) {
                        this.setToast("valid");
                        cookies.set("Auth", "Beta");
                        setTimeout(() => {
                            window.location.href = "../dashboard";
                        }, 2000);
                    } else {
                        this.setToast(true);
                        Swal.fire({
                            title: "Error",
                            text: "We can't validate your license",
                            icon: "error",
                        });
                    }
                });
        }
    };

    componentDidMount = () => {
        let Auth = cookies.get("Auth");
        if (Auth || Auth === "Beta") {
            window.location.href = "../../dashboard";
        }
    };

    render() {
        return (
            <>
                <div
                    className="formct d-flex justify-content-center align-items-center text-center"
                    style={{ minHeight: "100vh" }}
                >
                    <form
                        className="needs-validation bg-light rounded p-2"
                        style={{ maxWidth: "350px", minWidth: "320px" }}
                        onSubmit={this.handleSubmit}
                    >
                        <div className="mb-3">
                            <div className="d-flex justify-content-center">
                                <img
                                    src="../logos.png"
                                    width="100px"
                                    className="d-block"
                                    alt=""
                                />
                            </div>
                            <label
                                htmlFor="licensekey"
                                className="form-label text-primary"
                            >
                                License Key
                            </label>
                            <input
                                ref={this.licRef}
                                type="text"
                                className={`form-control ${
                                    this.state.toast === true
                                        ? ""
                                        : this.state.toast === "valid"
                                        ? "is-valid"
                                        : "is-invalid"
                                }`}
                                id="licensekey"
                            />
                            <div className="invalid-feedback">
                                Please enter your license key
                            </div>
                        </div>

                        <button type="submit" className="btn btn-success">
                            Log In
                        </button>
                    </form>
                </div>
                <div
                    className="toast-container position-absolute p-3 top-0 end-0"
                    id="toastPlacement"
                >
                    <div
                        style={{
                            opacity:
                                this.state.toast === true
                                    ? "0"
                                    : this.state.toast === "valid"
                                    ? "0"
                                    : "100%",
                        }}
                        className="toast align-items-center text-white bg-danger border-0"
                        role="alert"
                        aria-live="assertive"
                        aria-atomic="true"
                    >
                        <div className="d-flex">
                            <div className="toast-body">
                                Please fill the input field!!
                            </div>
                            <button
                                onClick={this.offToast}
                                type="button"
                                className="btn-close btn-close-white me-2 m-auto"
                                data-bs-dismiss="toast"
                                aria-label="Close"
                            ></button>
                        </div>
                    </div>
                </div>
                <div
                    className="toast-container position-absolute p-3 top-0 end-0"
                    id="toastPlacement"
                >
                    <div
                        style={{
                            opacity:
                                this.state.toast === true
                                    ? "0"
                                    : this.state.toast === "valid"
                                    ? "100%"
                                    : "",
                        }}
                        className="toast align-items-center text-white bg-success border-0"
                        role="alert"
                        aria-live="assertive"
                        aria-atomic="true"
                    >
                        <div className="d-flex">
                            <div className="toast-body">Loging in</div>
                            <button
                                onClick={this.offToast}
                                type="button"
                                className="btn-close btn-close-white me-2 m-auto"
                                data-bs-dismiss="toast"
                                aria-label="Close"
                            ></button>
                        </div>
                    </div>
                </div>
            </>
        );
    }
}

export default Form;
